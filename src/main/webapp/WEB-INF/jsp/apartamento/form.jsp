<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Apartamento</title>

<link href="<c:url value="/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
	<div class="container">
		<h2>Apartamento</h2>

		<br>

		<form action="${linkTo[ApartamentoController].adiciona(null)}" method="post">
			<div class="row">
				<label for="proprietario">Proprietario: </label>
				<select name="apartamento.proprietario.id">
					<c:forEach items="${proprietarios}" var="proprietario">
						<option value="${proprietario.getId()}">${proprietario.getNome()}</option>
					</c:forEach>
				</select>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<div class="form-group">
						<label for="numApto">N�m. do Apartamento</label> <input id="numApto"
							class="form-control" type="text" name="apartamento.numApto"
							value="${apartamento.numApto}">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="qtdQuartos">N�m. de Quartos</label> <input id="qtdQuartos"
							class="form-control" type="number" name="apartamento.qtdQuartos"
							value="${apartamento.qtdQuartos}">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="morador">Morador</label> <input id="morador"
							class="form-control" type="text" name="apartamento.morador"
							value="${apartamento.morador}">
					</div>
				</div>

				<div class="col-sm-3">
					<div class="form-group">
						<label for="ocupacao">Ocupa��o</label> <input id="ocupacao"
							class="form-control" type="text" name="apartamento.ocupacao"
							value="${apartamento.ocupacao}">
					</div>
				</div>
			</div>



			<div class="row">
				<div class="col-sm-12">
					<button class="btn btn-primary pull-right" type="submit">Salvar</button>
				</div>
			</div>
		</form>
	</div>

	<script src="<c:url value="/js/jquery.min.js" />"></script>
	<script src="<c:url value="/js/bootstrap.min.js" />"></script>
</body>
</html>