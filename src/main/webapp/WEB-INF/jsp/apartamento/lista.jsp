<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<a href="${linkTo[ApartamentoController].form()}">Novo Apartamento</a>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Num. Apto</th>
			<th>Num. de quartos</th>
			<th>Tipo Ocupação</th>
			<th>Morador</th>
			<th>Proprietário</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${apartamentos}" var="ap">
			<c:set var="id" value="${ap.id}" scope="request"></c:set>
			<tr>
				<td>${ap.id}</td>
				<td>${ap.numApto}</td>
				<td>${ap.qtdQuartos}</td>
				<td>${ap.ocupacao}</td>
				<td>${ap.morador}</td>
				<td>${ap.proprietario.nome}</td>

				<td><a href="${linkTo[ApartamentoController].edita(ap.id)}">
					<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</td>
				<td>
					  <form action="${linkTo[ApartamentoController].apaga(ap.id)}" method="POST">
					 <button class="btn btn-link" type="submit" name="_method" value="DELETE">
						<span class="glyphicon glyphicon-trash"></span>				 
					 </button>
					</form> 
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="/WEB-INF/jsp/footer.jsp" />