<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<title>Condomínio</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- ================================================================= -->
	<div class="container">
		<h2>Condomínio da Marina</h2>
		<ul class="nav nav-tabs" role="tablist">
			<li><a href="${linkTo[IndexController].index()}">Home</a></li>
			<li><a href="${linkTo[ProprietarioController].lista()}">Proprietario</a></li>
			<li><a href="${linkTo[ApartamentoController].lista()}">Apartamento</a></li>
			<li><a href="${linkTo[DespesaController].lista()}">Despesa</a></li>
			<li><a href="${linkTo[PagamentoController].lista()}">Pagamento</a></li>
		
		</ul>
	</div>

	<div class="container">
		<! class="col-sm-8">
		<!--  conteudo -->