<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<a href="${linkTo[DespesaController].form()}">Nova Despesa</a>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Descri��o</th>
			<th>Valor</th>
			<th>Editar</th>
			<th>Remover</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${despesas}" var="d">
			<c:set var="id" value="${d.id}" scope="request"></c:set>
			<tr>
				<td>${d.id}</td>
				<td>${d.descricao}</td>
				<td>${d.valor}</td>

				<td><a href="${linkTo[DespesaController].edita(d.id)}">
					<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</td>
				<td>
					  <form action="${linkTo[DespesaController].apaga(d.id)}" method="POST">
					 <button class="btn btn-link" type="submit" name="_method" value="DELETE">
						<span class="glyphicon glyphicon-trash"></span>				 
					 </button>
					</form> 
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="/WEB-INF/jsp/footer.jsp" />