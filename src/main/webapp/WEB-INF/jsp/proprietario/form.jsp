<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Proprietário</title>

    <link href="<c:url value="/css/bootstrap.min.css" />" rel="stylesheet">
  </head>
  <body>
  <div class="container">
    <h2>Proprietário</h2>
    
    <br>
    
	<form action="${linkTo[ProprietarioController].adiciona(null)}" method="post">
		<div class="row">
			<div class="col-sm-6">	
				<div class="form-group">
					<label for="nome">*Nome</label>
					<input id="nome" class="form-control" type="text" 
					name="proprietario.nome" value="${proprietario.nome}">
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label for="dataNascimento">*Data de nascimento</label>
					<input id="dataNascimento" class="form-control" type="text" 
					name="proprietario.dataNascimento" 
					value="<fmt:formatDate pattern="dd/MM/yyyy"  value="${proprietario.dataNascimento.time}"/>">
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="fone1">*Telefone</label>
					<input id="fone1" class="form-control" type="text" 
					name="proprietario.telefone" value="${proprietario.telefone}">
				</div>
			</div>
			
		</div>
		
		
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-primary pull-right" type="submit">Salvar</button>
			</div>
		</div>
	</form>
	</div>

    <script src="<c:url value="/js/jquery.min.js" />"></script>
    <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  </body>
</html>