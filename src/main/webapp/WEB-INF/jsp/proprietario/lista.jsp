<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<a href="${linkTo[ProprietarioController].form()}">Novo Proprietário</a>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Nome</th>
			<th>Telefone</th>
			<th>Editar</th>
			<th>Remover</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${proprietarios}" var="p">
			<c:set var="id" value="${p.id}" scope="request"></c:set>
			<tr>
				<td>${p.id}</td>
				<td>${p.nome}</td>
				<td>${p.telefone}</td>

				<td><a href="${linkTo[ProprietarioController].edita(p.id)}">
					<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</td>
				<td>
					  <form action="${linkTo[ProprietarioController].apaga(p.id)}" method="POST">
					 <button class="btn btn-link" type="submit" name="_method" value="DELETE">
						<span class="glyphicon glyphicon-trash"></span>				 
					 </button>
					</form> 
				</td>
			</tr>
		</c:forEach>
	</tbody><a href="${linkTo[ProprietarioController].lista()}">Listar Proprietários</a>
</table>

<c:import url="/WEB-INF/jsp/footer.jsp" />