<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Pagamento</title>

    <link href="<c:url value="/css/bootstrap.min.css" />" rel="stylesheet">
  </head>
  <body>
  <div class="container">
    <h2>Pagamento</h2>
    
    <br>
    
	<form action="${linkTo[PagamentoController].adiciona(null)}" method="post">
		<div class="row">
			<label for="proprietario">Proprietario: </label>
				<select name="apartamento.proprietario.id">
					<c:forEach items="${proprietarios}" var="proprietario">
						<option value="${proprietario.getId()}">${proprietario.getNome()}</option>
					</c:forEach>
				</select>
			
			<div class="col-sm-6">	
				<div class="form-group">
					<label for="vencimento">*Vencimento</label>
					<input id="vencimento" class="form-control" type="text" 
					name="pagamento.vencimento" value="${pagamento.vencimento}">
				</div>
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-sm-3">
				<div class="form-group">
					<label for="dataPagamento">*Data de pagamento</label>
					<input id="dataPagamento" class="form-control" type="text" 
					name="pagamento.dataPagamento" 
					value="<fmt:formatDate pattern="dd/MM/yyyy"  value="${pagamento.dataNascimento.time}"/>">
				</div>
			</div>
			
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="valorT">*Valor</label>
					<input id="valorT" class="form-control" type="number" 
					name="pagamento.valorT" value="${pagamento.valorT}">
				</div>
			</div>
			
			<div class="col-sm-3">	
				<div class="form-group">
					<label for="multa">Multa</label>
					<input id="multa" class="form-control" type="number" 
					name="pagamento.multa" value="${pagamento.multa}">
				</div>
				
				
			</div>
			
		</div>
		
		
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-primary pull-right" type="submit">Salvar</button>
			</div>
		</div>
	</form>
	</div>

    <script src="<c:url value="/js/jquery.min.js" />"></script>
    <script src="<c:url value="/js/bootstrap.min.js" />"></script>
  </body>
</html>