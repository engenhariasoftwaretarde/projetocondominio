<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<a href="${linkTo[PagamentoController].form()}">Novo Pagamento</a>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Proprietario</th>
			<th>Vencimento</th>
			<th>Multa</th>
			<th>Valor Total</th>
			<th>Data de Pagamento</th>
			<th>Editar</th>
			<th>Remover</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pagamentos}" var="p">
			<c:set var="id" value="${p.id}" scope="request"></c:set>
			<tr>
				<td>${p.id}</td>
				<td>${p.proprietario.nome}</td>
				<td>${p.vencimento}</td>
				<td>${p.multa}</td>
				<td>${p.valorT}</td>
				<td>${p.dataPagamento}</td>

				<td><a href="${linkTo[PagamentoController].edita(p.id)}">
					<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</td>
				<td>
					  <form action="${linkTo[PagamentoController].apaga(p.id)}" method="POST">
					 <button class="btn btn-link" type="submit" name="_method" value="DELETE">
						<span class="glyphicon glyphicon-trash"></span>				 
					 </button>
					</form> 
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="/WEB-INF/jsp/footer.jsp" />