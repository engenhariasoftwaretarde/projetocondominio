package br.com.fatec.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.fatec.modelo.Pagamento;

public class PagamentoDao {
	private EntityManager manager;

	@Inject
	public PagamentoDao(EntityManager manager) {
		this.manager = manager;
	}

	public PagamentoDao() {

	}

	public List<Pagamento> lista() {
		TypedQuery<Pagamento> query = manager.createQuery("select j from Pagamento j", Pagamento.class);
		return query.getResultList();

	}

	public void adiciona(Pagamento pagamento) {
		if (pagamento.getId() == 0) {
			manager.persist(pagamento);
		} else {
			manager.merge(pagamento);
		}
	}

	public Pagamento busca(long id) {
		return manager.find(Pagamento.class, id);
	}

	public void deleta(long id) {
		manager.remove(manager.getReference(Pagamento.class, id));
	}

}
