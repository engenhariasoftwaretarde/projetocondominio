package br.com.fatec.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.fatec.modelo.Despesa;

public class DespesaDao {
	private EntityManager manager;

	@Inject
	public DespesaDao(EntityManager manager) {
		this.manager = manager;
	}

	public DespesaDao() {

	}

	public List<Despesa> lista() {
		TypedQuery<Despesa> query = manager.createQuery("select j from Despesa j", Despesa.class);
		return query.getResultList();

	}

	public void adiciona(Despesa despesa) {
		if (despesa.getId() == 0) {
			manager.persist(despesa);
		} else {
			manager.merge(despesa);
		}
	}

	public Despesa busca(long id) {
		return manager.find(Despesa.class, id);
	}

	public void deleta(long id) {
		manager.remove(manager.getReference(Despesa.class, id));
	}

}
