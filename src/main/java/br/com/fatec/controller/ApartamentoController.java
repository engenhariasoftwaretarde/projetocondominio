package br.com.fatec.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.validator.Validator;
import br.com.fatec.dao.ApartamentoDao;
import br.com.fatec.dao.ProprietarioDao;
import br.com.fatec.modelo.Apartamento;

public class ApartamentoController {
	private ApartamentoDao apartamentoDao;
	private Validator validator;
	private Result result;
	private ProprietarioDao proprietarioDao;
	
	public ApartamentoController() {
	}

	@Inject
	public ApartamentoController(ApartamentoDao apartamentoDao, ProprietarioDao proprietarioDao, Validator validator, final Result result) {
		this.apartamentoDao = apartamentoDao;
		this.proprietarioDao = proprietarioDao;
		this.validator = validator;
		this.result = result;
	}

	public void form(){}
	
	@IncludeParameters
	@Post
	public void adiciona (Apartamento apartamento){
		validator.onErrorRedirectTo(this).form();
		apartamentoDao.adiciona(apartamento);
		result.redirectTo (this).form();
	}

	
	public void lista() {
		List<Apartamento> apartamentos = apartamentoDao.lista();
		result.include("apartamentoes", apartamentos);
		//result.redirectTo (this).lista();
	}
	
	@Get("/apartamento/{id}")
	public void edita (long id){
		Apartamento apartamento = apartamentoDao.busca(id);
		result.include("apartamento", apartamento);
		result.of(this).form();
				
	}
	@Delete("/apartamento/{id}")
	public void apaga (long id){
		apartamentoDao.deleta(id);
		result.redirectTo (this).lista();
				
	}
}


