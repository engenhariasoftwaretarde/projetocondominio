package br.com.fatec.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.validator.Validator;
import br.com.fatec.dao.ApartamentoDao;
import br.com.fatec.dao.DespesaDao;
import br.com.fatec.dao.ProprietarioDao;
import br.com.fatec.modelo.Despesa;

@Controller
public class DespesaController {
	private DespesaDao despesaDao;
	private Validator validator;
	private Result result;
	private ApartamentoDao apartamentoDao;

	public DespesaController() {
	}

	@Inject
	public DespesaController(DespesaDao despesaDao, ApartamentoDao apartamentoDao, Validator validator, Result result) {
		this.apartamentoDao= apartamentoDao;
		this.despesaDao = despesaDao;
		this.validator = validator;
		this.result = result;
	}

	public void form() {
	}

	@IncludeParameters
	@Post
	public void adiciona(Despesa despesa) {
		validator.onErrorRedirectTo(this).form();
		despesaDao.adiciona(despesa);
		result.redirectTo(this).form();
	}

	public void lista() {
		List<Despesa> despesas = despesaDao.lista();
		result.include("despesas", despesas);
		// result.redirectTo (this).lista();
	}

	@Get("/despesa/{id}")
	public void edita(long id) {
		Despesa despesa = despesaDao.busca(id);
		result.include("despesa", despesa);
		result.of(this).form();

	}

	@Delete("/despesa/{id}")
	public void apaga(long id) {
		despesaDao.deleta(id);
		result.redirectTo(this).lista();

	}
}

