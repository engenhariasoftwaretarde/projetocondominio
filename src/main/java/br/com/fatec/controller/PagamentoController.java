package br.com.fatec.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.validator.Validator;
import br.com.fatec.dao.PagamentoDao;
import br.com.fatec.dao.ProprietarioDao;
import br.com.fatec.modelo.Pagamento;

@Controller
public class PagamentoController {
	private PagamentoDao pagamentoDao;
	private Validator validator;
	private Result result;
	private ProprietarioDao proprietarioDao;

	public PagamentoController() {
	}

	@Inject
	public PagamentoController(PagamentoDao pagamentoDao, ProprietarioDao proprietarioDao, Validator validator, Result result) {
		this.proprietarioDao = proprietarioDao;
		this.pagamentoDao = pagamentoDao;
		this.validator = validator;
		this.result = result;
	}

	public void form() {
	}

	@IncludeParameters
	@Post
	public void adiciona(Pagamento pagamento) {
		validator.onErrorRedirectTo(this).form();
		pagamentoDao.adiciona(pagamento);
		result.redirectTo(this).form();
	}

	public void lista() {
		List<Pagamento> pagamentos = pagamentoDao.lista();
		result.include("pagamentos", pagamentos);
		// result.redirectTo (this).lista();
	}

	@Get("/pagamento/{id}")
	public void edita(long id) {
		Pagamento pagamento = pagamentoDao.busca(id);
		result.include("pagamento", pagamento);
		result.of(this).form();

	}

	@Delete("/pagamento/{id}")
	public void apaga(long id) {
		pagamentoDao.deleta(id);
		result.redirectTo(this).lista();

	}
}
